// vertex shader

struct InstanceInput {
    @location(2) model_matrix_0: vec4<f32>,
    @location(3) model_matrix_1: vec4<f32>,
    @location(4) model_matrix_2: vec4<f32>,
    @location(5) model_matrix_3: vec4<f32>,
};

struct WallRotationInput {
    @location(6) matrix_0: vec4<f32>,
    @location(7) matrix_1: vec4<f32>,
    @location(8) matrix_2: vec4<f32>,
    @location(9) matrix_3: vec4<f32>,
	@location(10) rotation_type: u32,
};

struct VertexInput {
	@location(0) vertex_position: vec3<f32>,
	@location(1) color: vec3<f32>,
};

struct VertexOutput {
	@builtin(position) clip_position: vec4<f32>,
	@location(0) color: vec3<f32>,
};

@group(0) @binding(0)
var<uniform> camera: mat4x4<f32>;

fn convert_to_color (color: u32) -> vec3<f32> {
	if color == u32(0) {
		return vec3<f32>(1.0, 1.0, 1.0); // white
	} else if color == u32(1) {
		return vec3<f32>(1.0, 1.0, 0.0); // yellow
	} else if color == u32(2) {
		return vec3<f32>(1.0, 0.0, 0.0); // red
	} else if color == u32(3) {
		return vec3<f32>(1.0, 0.64, 0.0); // orange
	} else if color == u32(4) {
		return vec3<f32>(0.0, 1.0, 0.0); // green
	} else {
		return vec3<f32>(0.0, 0.0, 1.0); // blue
	}
}

@vertex
fn vs_main (@builtin(vertex_index) in_vertex_index: u32, vertex_input: VertexInput,
instance: InstanceInput, wall_rotation: WallRotationInput) -> VertexOutput {

	var out: VertexOutput;

	let model_matrix = mat4x4<f32>(
		instance.model_matrix_0,
		instance.model_matrix_1,
		instance.model_matrix_2,
		instance.model_matrix_3,
	);

	let wall_rotation_matrix = mat4x4<f32>(
		wall_rotation.matrix_0,
		wall_rotation.matrix_1,
		wall_rotation.matrix_2,
		wall_rotation.matrix_3,
	);

	if wall_rotation.rotation_type == u32(0) { // U turn
		if in_vertex_index < u32(18) { // top level front wall
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(54) && in_vertex_index < u32(72) { // top level back wall
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(108) && in_vertex_index < u32(126) { // top level left wall
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(162) && in_vertex_index < u32(180) { // top level right wall
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(216) && in_vertex_index < u32(270) { // top wall
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else {
			out.clip_position = vec4<f32>(vertex_input.vertex_position, 1.0);
		}
	} else if wall_rotation.rotation_type == u32(1) { // D turn
		if in_vertex_index >= u32(36) && in_vertex_index <= u32(53) { // top level front wall
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(90) && in_vertex_index <= u32(107) { // top level back wall
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(144) && in_vertex_index <= u32(161) { // top level left wall
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(198) && in_vertex_index <= u32(215) { // top level right wall
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(270) && in_vertex_index <= u32(323) { // bottom wall
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else {
			out.clip_position = vec4<f32>(vertex_input.vertex_position, 1.0);
		}
	} else if wall_rotation.rotation_type == u32(2) { // R turn
		if in_vertex_index >= u32(12) && in_vertex_index <= u32(17) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(30) && in_vertex_index <= u32(35) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(48) && in_vertex_index <= u32(53) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(66) && in_vertex_index <= u32(71) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(84) && in_vertex_index <= u32(89) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(102) && in_vertex_index <= u32(107) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(162) && in_vertex_index <= u32(215) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(228) && in_vertex_index <= u32(233) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(246) && in_vertex_index <= u32(251) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(264) && in_vertex_index <= u32(269) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(282) && in_vertex_index <= u32(287) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(300) && in_vertex_index <= u32(305) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(318) && in_vertex_index <= u32(323) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else {
			out.clip_position = vec4<f32>(vertex_input.vertex_position, 1.0);
		}
	} else if wall_rotation.rotation_type == u32(3) { // L turn
		if in_vertex_index >= u32(0) && in_vertex_index <= u32(5) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(18) && in_vertex_index <= u32(23) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(36) && in_vertex_index <= u32(41) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(54) && in_vertex_index <= u32(59) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(72) && in_vertex_index <= u32(77) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(90) && in_vertex_index <= u32(95) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(108) && in_vertex_index <= u32(161) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(216) && in_vertex_index <= u32(221) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(234) && in_vertex_index <= u32(239) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(252) && in_vertex_index <= u32(257) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(270) && in_vertex_index <= u32(275) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(288) && in_vertex_index <= u32(293) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(306) && in_vertex_index <= u32(311) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else {
			out.clip_position = vec4<f32>(vertex_input.vertex_position, 1.0);
		}
	} else if wall_rotation.rotation_type == u32(4) { // F turn
		if in_vertex_index >= u32(0) && in_vertex_index <= u32(53) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(120) && in_vertex_index <= u32(125) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(138) && in_vertex_index <= u32(143) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(156) && in_vertex_index <= u32(161) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(174) && in_vertex_index <= u32(179) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(192) && in_vertex_index <= u32(197) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(210) && in_vertex_index <= u32(233) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(270) && in_vertex_index <= u32(287) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else {
			out.clip_position = vec4<f32>(vertex_input.vertex_position, 1.0);
		}
	} else if wall_rotation.rotation_type == u32(5) { // B turn
		if in_vertex_index >= u32(54) && in_vertex_index <= u32(113) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(126) && in_vertex_index <= u32(131) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(144) && in_vertex_index <= u32(149) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(162) && in_vertex_index <= u32(167) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(180) && in_vertex_index <= u32(185) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(198) && in_vertex_index <= u32(203) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(252) && in_vertex_index <= u32(269) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else if in_vertex_index >= u32(306) && in_vertex_index <= u32(323) {
			out.clip_position = wall_rotation_matrix * vec4<f32>(vertex_input.vertex_position, 1.0);
		} else {
			out.clip_position = vec4<f32>(vertex_input.vertex_position, 1.0);
		}
	} else {
		out.clip_position = vec4<f32>(vertex_input.vertex_position, 1.0);
	}

	out.clip_position = camera * model_matrix * out.clip_position;
	out.color = vertex_input.color;

    return out;
}

// fragment shader

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
		return vec4<f32>(in.color, 1.0);
}
