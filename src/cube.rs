use crate::{
    vertex::{Vertex, VERTICES_COUNT},
    WallRotation,
};
use cgmath::prelude::*;
use std::collections::VecDeque;
use std::time::Duration;
use wgpu::util::DeviceExt;

pub const ROTATION_SPEED: f32 = 420.0;
pub const FACE_ROTATION_SPEED: f32 = 500.0;

pub enum Rotation {
    U,
    D,
    R,
    L,
    F,
    B,
}

pub struct Cube {
    pub vertices: [Vertex; VERTICES_COUNT],
    pub rotation: cgmath::Quaternion<f32>,
    pub face_rotation_queue: VecDeque<Rotation>,
    pub angle: f32,
    pub rotate_forward: bool,
    pub rotate_backward: bool,
    pub rotate_turn_left: bool,  //top to bottom axis
    pub rotate_turn_right: bool, //top to bottom axis
    pub rotate_spin_left: bool,  //front to back axis
    pub rotate_spin_right: bool, //front to back axis
}

impl Cube {
    pub fn new() -> Self {
        let vertices = crate::vertex::new();
        let rotation: cgmath::Quaternion<f32> =
            cgmath::Quaternion::from_axis_angle(cgmath::Vector3::unit_x(), cgmath::Deg(0.0));
        let face_rotation_queue = VecDeque::new();
        let angle = 0.0;
        Cube {
            vertices,
            rotation,
            face_rotation_queue,
            angle,
            rotate_forward: false,
            rotate_backward: false,
            rotate_turn_left: false,
            rotate_turn_right: false,
            rotate_spin_left: false,
            rotate_spin_right: false,
        }
    }

    pub fn rotate(
        &mut self,
        device: &wgpu::Device,
        instance_buffer: &mut wgpu::Buffer,
        frame_duration: Duration,
    ) {
        let rotation_angle = ROTATION_SPEED * frame_duration.as_secs_f32();
        if self.rotate_forward {
            self.rotation = cgmath::Quaternion::from_axis_angle(
                cgmath::Vector3::unit_x(),
                cgmath::Deg(-rotation_angle),
            ) * self.rotation;
            let model: [[f32; 4]; 4] = (cgmath::Matrix4::from(self.rotation)).into();

            *instance_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Instance Buffer"),
                contents: bytemuck::cast_slice(&model),
                usage: wgpu::BufferUsages::VERTEX,
            });
        }
        if self.rotate_backward {
            self.rotation = cgmath::Quaternion::from_axis_angle(
                cgmath::Vector3::unit_x(),
                cgmath::Deg(rotation_angle),
            ) * self.rotation;
            let model: [[f32; 4]; 4] = (cgmath::Matrix4::from(self.rotation)).into();

            *instance_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Instance Buffer"),
                contents: bytemuck::cast_slice(&model),
                usage: wgpu::BufferUsages::VERTEX,
            });
        }
        if self.rotate_turn_left {
            self.rotation = cgmath::Quaternion::from_axis_angle(
                cgmath::Vector3::unit_y(),
                cgmath::Deg(-rotation_angle),
            ) * self.rotation;
            let model: [[f32; 4]; 4] = (cgmath::Matrix4::from(self.rotation)).into();

            *instance_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Instance Buffer"),
                contents: bytemuck::cast_slice(&model),
                usage: wgpu::BufferUsages::VERTEX,
            });
        }
        if self.rotate_turn_right {
            self.rotation = cgmath::Quaternion::from_axis_angle(
                cgmath::Vector3::unit_y(),
                cgmath::Deg(rotation_angle),
            ) * self.rotation;
            let model: [[f32; 4]; 4] = (cgmath::Matrix4::from(self.rotation)).into();

            *instance_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Instance Buffer"),
                contents: bytemuck::cast_slice(&model),
                usage: wgpu::BufferUsages::VERTEX,
            });
        }
        if self.rotate_spin_left {
            self.rotation = cgmath::Quaternion::from_axis_angle(
                cgmath::Vector3::unit_z(),
                cgmath::Deg(rotation_angle),
            ) * self.rotation;
            let model: [[f32; 4]; 4] = (cgmath::Matrix4::from(self.rotation)).into();

            *instance_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Instance Buffer"),
                contents: bytemuck::cast_slice(&model),
                usage: wgpu::BufferUsages::VERTEX,
            });
        }
        if self.rotate_spin_right {
            self.rotation = cgmath::Quaternion::from_axis_angle(
                cgmath::Vector3::unit_z(),
                cgmath::Deg(-rotation_angle),
            ) * self.rotation;
            let model: [[f32; 4]; 4] = (cgmath::Matrix4::from(self.rotation)).into();

            *instance_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Instance Buffer"),
                contents: bytemuck::cast_slice(&model),
                usage: wgpu::BufferUsages::VERTEX,
            });
        }
    }

    fn update_wall_rotation_buffer(
        &mut self,
        device: &wgpu::Device,
        wall_rotation_buffer: &mut wgpu::Buffer,
        rotation_type: u32,
    ) {
        let rotate_wall = match rotation_type {
            0 | 1 => cgmath::Quaternion::from_axis_angle(
                cgmath::Vector3::unit_y(),
                cgmath::Deg(self.angle),
            ),
            2 | 3 => cgmath::Quaternion::from_axis_angle(
                cgmath::Vector3::unit_x(),
                cgmath::Deg(self.angle),
            ),
            4 | 5 => cgmath::Quaternion::from_axis_angle(
                cgmath::Vector3::unit_z(),
                cgmath::Deg(self.angle),
            ),
            _ => cgmath::Quaternion::from_axis_angle(
                // should never happen
                cgmath::Vector3::unit_y(),
                cgmath::Deg(self.angle),
            ),
        };
        let wall_rotation: [[f32; 4]; 4] = cgmath::Matrix4::from(rotate_wall).into();
        let wall_rotation = WallRotation {
            wall_rotation,
            rotation_type,
        };

        *wall_rotation_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Wall Rotation Buffer"),
            contents: bytemuck::cast_slice(&[wall_rotation]),
            usage: wgpu::BufferUsages::VERTEX,
        });
    }

    pub fn rotate_face(
        &mut self,
        device: &wgpu::Device,
        wall_rotation_buffer: &mut wgpu::Buffer,
        vertex_buffer: &mut wgpu::Buffer,
        frame_duration: Duration,
    ) {
        let rotation_speed = FACE_ROTATION_SPEED * frame_duration.as_secs_f32();

        match self.face_rotation_queue.front() {
            Some(Rotation::U) => {
                let rotation_type = 0;
                self.angle += rotation_speed;
                self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);
                if self.angle + rotation_speed >= 90.0 {
                    self.face_rotation_queue.pop_front();
                    self.angle = 0.0;
                    self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);

                    // front wall
                    let tmp_color_1 = self.vertices[0].color;
                    let tmp_color_2 = self.vertices[6].color;
                    let tmp_color_3 = self.vertices[12].color;
                    for i in 0..6 {
                        self.vertices[i].color = self.vertices[108].color;
                    }
                    for i in 6..12 {
                        self.vertices[i].color = self.vertices[114].color;
                    }
                    for i in 12..18 {
                        self.vertices[i].color = self.vertices[120].color;
                    }
                    // left wall
                    for i in 108..114 {
                        self.vertices[i].color = self.vertices[66].color;
                    }
                    for i in 114..120 {
                        self.vertices[i].color = self.vertices[60].color;
                    }
                    for i in 120..126 {
                        self.vertices[i].color = self.vertices[54].color;
                    }
                    // back wall
                    for i in 54..60 {
                        self.vertices[i].color = self.vertices[162].color;
                    }
                    for i in 60..66 {
                        self.vertices[i].color = self.vertices[168].color;
                    }
                    for i in 66..72 {
                        self.vertices[i].color = self.vertices[174].color;
                    }
                    // right wall
                    for i in 162..168 {
                        self.vertices[i].color = tmp_color_3;
                    }
                    for i in 168..174 {
                        self.vertices[i].color = tmp_color_2;
                    }
                    for i in 174..180 {
                        self.vertices[i].color = tmp_color_1;
                    }
                    // top wall
                    let tmp_color = self.vertices[216].color; // corner
                    for i in 216..222 {
                        self.vertices[i].color = self.vertices[252].color;
                    }
                    for i in 252..258 {
                        self.vertices[i].color = self.vertices[264].color;
                    }
                    for i in 264..270 {
                        self.vertices[i].color = self.vertices[228].color;
                    }
                    for i in 228..234 {
                        self.vertices[i].color = tmp_color;
                    }

                    let tmp_color = self.vertices[222].color; // middle
                    for i in 222..228 {
                        self.vertices[i].color = self.vertices[234].color;
                    }
                    for i in 234..240 {
                        self.vertices[i].color = self.vertices[258].color;
                    }
                    for i in 258..264 {
                        self.vertices[i].color = self.vertices[246].color;
                    }
                    for i in 246..252 {
                        self.vertices[i].color = tmp_color;
                    }

                    *vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                        label: Some("Vertex Buffer"),
                        contents: bytemuck::cast_slice(&self.vertices),
                        usage: wgpu::BufferUsages::VERTEX,
                    });
                }
            }
            Some(Rotation::D) => {
                let rotation_type = 1;
                self.angle += rotation_speed;
                self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);

                if self.angle + rotation_speed >= 90.0 {
                    self.face_rotation_queue.pop_front();
                    self.angle = 0.0;
                    self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);

                    // front wall
                    let tmp_color_1 = self.vertices[36].color;
                    let tmp_color_2 = self.vertices[42].color;
                    let tmp_color_3 = self.vertices[48].color;
                    for i in 36..42 {
                        self.vertices[i].color = self.vertices[144].color;
                    }
                    for i in 42..48 {
                        self.vertices[i].color = self.vertices[150].color;
                    }
                    for i in 48..54 {
                        self.vertices[i].color = self.vertices[156].color;
                    }
                    // left wall
                    for i in 144..150 {
                        self.vertices[i].color = self.vertices[102].color;
                    }
                    for i in 150..156 {
                        self.vertices[i].color = self.vertices[96].color;
                    }
                    for i in 156..162 {
                        self.vertices[i].color = self.vertices[90].color;
                    }
                    // back wall
                    for i in 90..96 {
                        self.vertices[i].color = self.vertices[198].color;
                    }
                    for i in 96..102 {
                        self.vertices[i].color = self.vertices[204].color;
                    }
                    for i in 102..108 {
                        self.vertices[i].color = self.vertices[210].color;
                    }
                    // right wall
                    for i in 198..204 {
                        self.vertices[i].color = tmp_color_3;
                    }
                    for i in 204..210 {
                        self.vertices[i].color = tmp_color_2;
                    }
                    for i in 210..216 {
                        self.vertices[i].color = tmp_color_1;
                    }
                    // bottom wall
                    let tmp_color = self.vertices[270].color; // corner
                    for i in 270..276 {
                        self.vertices[i].color = self.vertices[306].color;
                    }
                    for i in 306..312 {
                        self.vertices[i].color = self.vertices[318].color;
                    }
                    for i in 318..324 {
                        self.vertices[i].color = self.vertices[282].color;
                    }
                    for i in 282..288 {
                        self.vertices[i].color = tmp_color;
                    }

                    let tmp_color = self.vertices[276].color; // middle
                    for i in 276..282 {
                        self.vertices[i].color = self.vertices[288].color;
                    }
                    for i in 288..294 {
                        self.vertices[i].color = self.vertices[312].color;
                    }
                    for i in 312..318 {
                        self.vertices[i].color = self.vertices[300].color;
                    }
                    for i in 300..306 {
                        self.vertices[i].color = tmp_color;
                    }

                    *vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                        label: Some("Vertex Buffer"),
                        contents: bytemuck::cast_slice(&self.vertices),
                        usage: wgpu::BufferUsages::VERTEX,
                    });
                }
            }
            Some(Rotation::R) => {
                let rotation_type = 2;
                self.angle -= rotation_speed;
                self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);

                if self.angle - rotation_speed <= -90.0 {
                    self.face_rotation_queue.pop_front();
                    self.angle = 0.0;
                    self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);

                    // front wall
                    let tmp_color_1 = self.vertices[12].color;
                    let tmp_color_2 = self.vertices[30].color;
                    let tmp_color_3 = self.vertices[48].color;
                    for i in 12..18 {
                        self.vertices[i].color = self.vertices[282].color;
                    }
                    for i in 30..36 {
                        self.vertices[i].color = self.vertices[300].color;
                    }
                    for i in 48..54 {
                        self.vertices[i].color = self.vertices[318].color;
                    }
                    // bottom wall
                    for i in 282..288 {
                        self.vertices[i].color = self.vertices[102].color;
                    }
                    for i in 300..306 {
                        self.vertices[i].color = self.vertices[84].color;
                    }
                    for i in 318..324 {
                        self.vertices[i].color = self.vertices[66].color;
                    }
                    // back wall
                    for i in 66..72 {
                        self.vertices[i].color = self.vertices[228].color;
                    }
                    for i in 84..90 {
                        self.vertices[i].color = self.vertices[246].color;
                    }
                    for i in 102..108 {
                        self.vertices[i].color = self.vertices[264].color;
                    }
                    // top wall
                    for i in 228..234 {
                        self.vertices[i].color = tmp_color_3;
                    }
                    for i in 246..252 {
                        self.vertices[i].color = tmp_color_2;
                    }
                    for i in 264..270 {
                        self.vertices[i].color = tmp_color_1;
                    }
                    // right wall
                    let tmp_color = self.vertices[174].color; // corner
                    for i in 174..180 {
                        self.vertices[i].color = self.vertices[210].color;
                    }
                    for i in 210..216 {
                        self.vertices[i].color = self.vertices[198].color;
                    }
                    for i in 198..204 {
                        self.vertices[i].color = self.vertices[162].color;
                    }
                    for i in 162..168 {
                        self.vertices[i].color = tmp_color;
                    }

                    let tmp_color = self.vertices[168].color; // middle
                    for i in 168..174 {
                        self.vertices[i].color = self.vertices[192].color;
                    }
                    for i in 192..198 {
                        self.vertices[i].color = self.vertices[204].color;
                    }
                    for i in 204..210 {
                        self.vertices[i].color = self.vertices[180].color;
                    }
                    for i in 180..186 {
                        self.vertices[i].color = tmp_color;
                    }

                    *vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                        label: Some("Vertex Buffer"),
                        contents: bytemuck::cast_slice(&self.vertices),
                        usage: wgpu::BufferUsages::VERTEX,
                    });
                }
            }
            Some(Rotation::L) => {
                let rotation_type = 3;
                self.angle += rotation_speed;
                self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);
                if self.angle + rotation_speed >= 90.0 {
                    self.face_rotation_queue.pop_front();
                    self.angle = 0.0;
                    self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);

                    // front wall
                    let tmp_color_1 = self.vertices[0].color;
                    let tmp_color_2 = self.vertices[18].color;
                    let tmp_color_3 = self.vertices[36].color;
                    for i in 0..6 {
                        self.vertices[i].color = self.vertices[252].color;
                    }
                    for i in 18..24 {
                        self.vertices[i].color = self.vertices[234].color;
                    }
                    for i in 36..42 {
                        self.vertices[i].color = self.vertices[216].color;
                    }
                    // top wall
                    for i in 252..258 {
                        self.vertices[i].color = self.vertices[90].color;
                    }
                    for i in 234..240 {
                        self.vertices[i].color = self.vertices[72].color;
                    }
                    for i in 216..222 {
                        self.vertices[i].color = self.vertices[54].color;
                    }
                    // back wall
                    for i in 54..60 {
                        self.vertices[i].color = self.vertices[306].color;
                    }
                    for i in 72..78 {
                        self.vertices[i].color = self.vertices[288].color;
                    }
                    for i in 90..96 {
                        self.vertices[i].color = self.vertices[270].color;
                    }
                    // bottom wall
                    for i in 306..312 {
                        self.vertices[i].color = tmp_color_3;
                    }
                    for i in 288..294 {
                        self.vertices[i].color = tmp_color_2;
                    }
                    for i in 270..276 {
                        self.vertices[i].color = tmp_color_1;
                    }
                    // left wall
                    let tmp_color = self.vertices[108].color; // corner
                    for i in 108..114 {
                        self.vertices[i].color = self.vertices[144].color;
                    }
                    for i in 144..150 {
                        self.vertices[i].color = self.vertices[156].color;
                    }
                    for i in 156..162 {
                        self.vertices[i].color = self.vertices[120].color;
                    }
                    for i in 120..126 {
                        self.vertices[i].color = tmp_color;
                    }

                    let tmp_color = self.vertices[114].color; // middle
                    for i in 114..120 {
                        self.vertices[i].color = self.vertices[126].color;
                    }
                    for i in 126..132 {
                        self.vertices[i].color = self.vertices[150].color;
                    }
                    for i in 150..156 {
                        self.vertices[i].color = self.vertices[138].color;
                    }
                    for i in 138..144 {
                        self.vertices[i].color = tmp_color;
                    }

                    *vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                        label: Some("Vertex Buffer"),
                        contents: bytemuck::cast_slice(&self.vertices),
                        usage: wgpu::BufferUsages::VERTEX,
                    });
                }
            }
            Some(Rotation::F) => {
                let rotation_type = 4;
                self.angle -= rotation_speed;
                self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);
                if self.angle - rotation_speed <= -90.0 {
                    self.face_rotation_queue.pop_front();
                    self.angle = 0.0;
                    self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);

                    // top wall
                    let tmp_color_1 = self.vertices[216].color;
                    let tmp_color_2 = self.vertices[222].color;
                    let tmp_color_3 = self.vertices[228].color;
                    for i in 216..222 {
                        self.vertices[i].color = self.vertices[156].color;
                    }
                    for i in 222..228 {
                        self.vertices[i].color = self.vertices[138].color;
                    }
                    for i in 228..234 {
                        self.vertices[i].color = self.vertices[120].color;
                    }
                    // left wall
                    for i in 120..126 {
                        self.vertices[i].color = self.vertices[270].color;
                    }
                    for i in 138..144 {
                        self.vertices[i].color = self.vertices[276].color;
                    }
                    for i in 156..162 {
                        self.vertices[i].color = self.vertices[282].color;
                    }
                    // bottom wall
                    for i in 270..276 {
                        self.vertices[i].color = self.vertices[210].color;
                    }
                    for i in 276..282 {
                        self.vertices[i].color = self.vertices[192].color;
                    }
                    for i in 282..288 {
                        self.vertices[i].color = self.vertices[174].color;
                    }
                    // right wall
                    for i in 210..216 {
                        self.vertices[i].color = tmp_color_3;
                    }
                    for i in 192..198 {
                        self.vertices[i].color = tmp_color_2;
                    }
                    for i in 174..180 {
                        self.vertices[i].color = tmp_color_1;
                    }
                    // front wall
                    let tmp_color = self.vertices[0].color; // corner
                    for i in 0..6 {
                        self.vertices[i].color = self.vertices[36].color;
                    }
                    for i in 36..42 {
                        self.vertices[i].color = self.vertices[48].color;
                    }
                    for i in 48..54 {
                        self.vertices[i].color = self.vertices[12].color;
                    }
                    for i in 12..18 {
                        self.vertices[i].color = tmp_color;
                    }

                    let tmp_color = self.vertices[6].color; // middle
                    for i in 6..12 {
                        self.vertices[i].color = self.vertices[18].color;
                    }
                    for i in 18..24 {
                        self.vertices[i].color = self.vertices[42].color;
                    }
                    for i in 42..48 {
                        self.vertices[i].color = self.vertices[30].color;
                    }
                    for i in 30..36 {
                        self.vertices[i].color = tmp_color;
                    }

                    *vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                        label: Some("Vertex Buffer"),
                        contents: bytemuck::cast_slice(&self.vertices),
                        usage: wgpu::BufferUsages::VERTEX,
                    });
                }
            }
            Some(Rotation::B) => {
                let rotation_type = 5;
                self.angle += rotation_speed;
                self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);
                if self.angle + rotation_speed >= 90.0 {
                    self.face_rotation_queue.pop_front();
                    self.angle = 0.0;
                    self.update_wall_rotation_buffer(device, wall_rotation_buffer, rotation_type);

                    // top wall
                    let tmp_color_1 = self.vertices[252].color;
                    let tmp_color_2 = self.vertices[258].color;
                    let tmp_color_3 = self.vertices[264].color;
                    for i in 252..258 {
                        self.vertices[i].color = self.vertices[162].color;
                    }
                    for i in 258..264 {
                        self.vertices[i].color = self.vertices[180].color;
                    }
                    for i in 264..270 {
                        self.vertices[i].color = self.vertices[198].color;
                    }
                    // right wall
                    for i in 162..168 {
                        self.vertices[i].color = self.vertices[318].color;
                    }
                    for i in 180..186 {
                        self.vertices[i].color = self.vertices[312].color;
                    }
                    for i in 198..204 {
                        self.vertices[i].color = self.vertices[306].color;
                    }
                    // bottom wall
                    for i in 318..324 {
                        self.vertices[i].color = self.vertices[144].color;
                    }
                    for i in 312..318 {
                        self.vertices[i].color = self.vertices[126].color;
                    }
                    for i in 306..312 {
                        self.vertices[i].color = self.vertices[108].color;
                    }
                    // left wall
                    for i in 144..150 {
                        self.vertices[i].color = tmp_color_1;
                    }
                    for i in 126..132 {
                        self.vertices[i].color = tmp_color_2;
                    }
                    for i in 108..114 {
                        self.vertices[i].color = tmp_color_3;
                    }
                    // back wall
                    let tmp_color = self.vertices[54].color; // corner
                    for i in 54..60 {
                        self.vertices[i].color = self.vertices[66].color;
                    }
                    for i in 66..72 {
                        self.vertices[i].color = self.vertices[102].color;
                    }
                    for i in 102..108 {
                        self.vertices[i].color = self.vertices[90].color;
                    }
                    for i in 90..96 {
                        self.vertices[i].color = tmp_color;
                    }

                    let tmp_color = self.vertices[60].color; // middle
                    for i in 60..66 {
                        self.vertices[i].color = self.vertices[84].color;
                    }
                    for i in 84..90 {
                        self.vertices[i].color = self.vertices[96].color;
                    }
                    for i in 96..102 {
                        self.vertices[i].color = self.vertices[72].color;
                    }
                    for i in 72..78 {
                        self.vertices[i].color = tmp_color;
                    }

                    *vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                        label: Some("Vertex Buffer"),
                        contents: bytemuck::cast_slice(&self.vertices),
                        usage: wgpu::BufferUsages::VERTEX,
                    });
                }
            }
            _ => {}
        }
    }
}
