use crate::{cube::Rotation, Renderer};
use winit::{
    dpi::PhysicalSize,
    event::{ElementState, Event, KeyEvent, WindowEvent},
    event_loop::EventLoopWindowTarget,
    keyboard::{KeyCode, PhysicalKey},
};

fn handle_window_resized(physical_size: PhysicalSize<u32>, renderer: &mut Renderer) {
    renderer.surface_config.width = physical_size.width;
    renderer.surface_config.height = physical_size.height;
    renderer
        .surface
        .configure(&renderer.device, &renderer.surface_config);

    let depth_texture_size = wgpu::Extent3d {
        width: physical_size.width,
        height: physical_size.height,
        depth_or_array_layers: 1,
    };
    let depth_texture_desc = wgpu::TextureDescriptor {
        label: Some("depth_texture"),
        size: depth_texture_size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Depth32Float,
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::TEXTURE_BINDING,
        view_formats: &[],
    };
    let depth_texture = renderer.device.create_texture(&depth_texture_desc);
    renderer.depth_texture_view =
        depth_texture.create_view(&wgpu::TextureViewDescriptor::default());
}

fn handle_keboard_pressed(code: KeyCode, renderer: &mut Renderer) {
    match code {
        KeyCode::KeyQ => renderer.cube.face_rotation_queue.push_back(Rotation::U),
        KeyCode::KeyA => renderer.cube.face_rotation_queue.push_back(Rotation::D),
        KeyCode::KeyW => renderer.cube.face_rotation_queue.push_back(Rotation::R),
        KeyCode::KeyS => renderer.cube.face_rotation_queue.push_back(Rotation::L),
        KeyCode::KeyE => renderer.cube.face_rotation_queue.push_back(Rotation::F),
        KeyCode::KeyD => renderer.cube.face_rotation_queue.push_back(Rotation::B),
        KeyCode::KeyJ => {
            if !renderer.cube.rotate_turn_left {
                renderer.cube.rotate_turn_left = true;
            }
        }
        KeyCode::KeyL => {
            if !renderer.cube.rotate_turn_right {
                renderer.cube.rotate_turn_right = true;
            }
        }
        KeyCode::KeyK => {
            if !renderer.cube.rotate_backward {
                renderer.cube.rotate_backward = true;
            }
        }
        KeyCode::KeyI => {
            if !renderer.cube.rotate_forward {
                renderer.cube.rotate_forward = true;
            }
        }
        KeyCode::KeyU => {
            if !renderer.cube.rotate_spin_left {
                renderer.cube.rotate_spin_left = true;
            }
        }
        KeyCode::KeyO => {
            if !renderer.cube.rotate_spin_right {
                renderer.cube.rotate_spin_right = true;
            }
        }
        _ => (),
    }
}

fn handle_keyboard_released(code: KeyCode, renderer: &mut Renderer) {
    match code {
        KeyCode::KeyJ => renderer.cube.rotate_turn_left = false,
        KeyCode::KeyL => renderer.cube.rotate_turn_right = false,
        KeyCode::KeyI => renderer.cube.rotate_forward = false,
        KeyCode::KeyK => renderer.cube.rotate_backward = false,
        KeyCode::KeyU => renderer.cube.rotate_spin_left = false,
        KeyCode::KeyO => renderer.cube.rotate_spin_right = false,
        _ => (),
    }
}

pub fn handle_events(
    event: winit::event::Event<()>,
    target: &EventLoopWindowTarget<()>,
    renderer: &mut Renderer,
) {
    if let Event::WindowEvent { event, .. } = event {
        match event {
            WindowEvent::CloseRequested => target.exit(),
            WindowEvent::Resized(physical_size) => handle_window_resized(physical_size, renderer),
            WindowEvent::KeyboardInput {
                event:
                    KeyEvent {
                        state: ElementState::Pressed,
                        physical_key: PhysicalKey::Code(code),
                        ..
                    },
                ..
            } => handle_keboard_pressed(code, renderer),
            WindowEvent::KeyboardInput {
                event:
                    KeyEvent {
                        state: ElementState::Released,
                        physical_key: PhysicalKey::Code(code),
                        ..
                    },
                ..
            } => handle_keyboard_released(code, renderer),
            _ => (),
        }
    }
}
