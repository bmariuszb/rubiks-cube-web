mod cube;
mod event;
mod vertex;
mod window;

use crate::{
    cube::Cube,
    vertex::{Vertex, VERTICES_COUNT},
};
use cgmath::prelude::*;
use std::cell::RefCell;
use std::mem;
use std::rc::Rc;
use std::time::Duration;
use wasm_bindgen::prelude::*;
use wasm_timer::Instant;
use wgpu::{
    include_wgsl, util::DeviceExt, Adapter, Backends, Instance, InstanceDescriptor, StoreOp,
    Surface,
};
use winit::event_loop::EventLoop;

#[rustfmt::skip]
pub const OPENGL_TO_WGPU_MATRIX: cgmath::Matrix4<f32> = cgmath::Matrix4::new(
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.0, 0.0, 0.5, 1.0,
);

fn instance_new() -> Instance {
    let instance_descriptor = InstanceDescriptor {
        backends: Backends::GL,
        ..Default::default()
    };
    Instance::new(instance_descriptor)
}

fn adapter_new(instance: &Instance, surface: &Surface) -> Adapter {
    let adapter = async {
        instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::HighPerformance,
                force_fallback_adapter: false,
                compatible_surface: Some(surface),
            })
            .await
            .unwrap()
    };
    pollster::block_on(adapter)
}

fn device_queue_new(adapter: &Adapter) -> (wgpu::Device, wgpu::Queue) {
    let device_queue = async {
        adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    label: None,
                    required_features: wgpu::Features::empty(),
                    required_limits: wgpu::Limits::downlevel_webgl2_defaults()
                        .using_resolution(adapter.limits()),
                },
                None,
            )
            .await
            .unwrap()
    };
    pollster::block_on(device_queue)
}

fn vertex_buffer_new(device: &wgpu::Device, vertices: &[Vertex; VERTICES_COUNT]) -> wgpu::Buffer {
    device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Vertex Buffer"),
        contents: bytemuck::cast_slice(vertices),
        usage: wgpu::BufferUsages::VERTEX,
    })
}

fn camera_buffer_new(device: &wgpu::Device) -> wgpu::Buffer {
    let camera = Camera::new();
    let view = cgmath::Matrix4::look_at_rh(camera.eye, camera.target, camera.up);
    let proj = cgmath::perspective(
        cgmath::Deg(camera.fovy),
        camera.aspect,
        camera.znear,
        camera.zfar,
    );
    let camera: [[f32; 4]; 4] = (OPENGL_TO_WGPU_MATRIX * proj * view).into();

    device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Camera Buffer"),
        contents: bytemuck::cast_slice(&[camera]),
        usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
    })
}

fn camera_bind_group_layout_new(device: &wgpu::Device) -> wgpu::BindGroupLayout {
    device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
        entries: &[wgpu::BindGroupLayoutEntry {
            binding: 0,
            visibility: wgpu::ShaderStages::VERTEX,
            ty: wgpu::BindingType::Buffer {
                ty: wgpu::BufferBindingType::Uniform,
                has_dynamic_offset: false,
                min_binding_size: None,
            },
            count: None,
        }],
        label: Some("camera_bind_group_layout"),
    })
}

fn camera_bind_group_new(device: &wgpu::Device) -> wgpu::BindGroup {
    let camera_buffer = camera_buffer_new(device);

    device.create_bind_group(&wgpu::BindGroupDescriptor {
        layout: &camera_bind_group_layout_new(device),
        entries: &[wgpu::BindGroupEntry {
            binding: 0,
            resource: camera_buffer.as_entire_binding(),
        }],
        label: Some("camera_bind_group"),
    })
}

fn wall_rotation_buffer_new(device: &wgpu::Device) -> wgpu::Buffer {
    let rotate_wall: cgmath::Quaternion<f32> =
        cgmath::Quaternion::from_axis_angle(cgmath::Vector3::unit_z(), cgmath::Deg(0.0));
    let wall_rotation: [[f32; 4]; 4] = cgmath::Matrix4::from(rotate_wall).into();
    let wall_rotation = WallRotation {
        wall_rotation,
        rotation_type: 9,
    };

    device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Wall Rotation Buffer"),
        contents: bytemuck::cast_slice(&[wall_rotation]),
        usage: wgpu::BufferUsages::VERTEX,
    })
}

fn instance_buffer_new(device: &wgpu::Device, rotation: &cgmath::Quaternion<f32>) -> wgpu::Buffer {
    let model: [[f32; 4]; 4] = cgmath::Matrix4::from(*rotation).into();

    device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Instance Buffer"),
        contents: bytemuck::cast_slice(&model),
        usage: wgpu::BufferUsages::VERTEX,
    })
}

fn depth_texture_view_new(device: &wgpu::Device) -> wgpu::TextureView {
    let depth_texture_size = wgpu::Extent3d {
        width: *window::CANVAS_HEIGHT,
        height: *window::CANVAS_HEIGHT,
        depth_or_array_layers: 1,
    };

    let depth_texture_desc = wgpu::TextureDescriptor {
        label: Some("depth_texture"),
        size: depth_texture_size,
        mip_level_count: 1,
        sample_count: 1,
        dimension: wgpu::TextureDimension::D2,
        format: wgpu::TextureFormat::Depth32Float,
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::TEXTURE_BINDING,
        view_formats: &[],
    };

    let depth_texture = device.create_texture(&depth_texture_desc);
    depth_texture.create_view(&wgpu::TextureViewDescriptor::default())
}

fn wall_rotation_buffer_layout() -> wgpu::VertexBufferLayout<'static> {
    wgpu::VertexBufferLayout {
        array_stride: mem::size_of::<WallRotation>() as wgpu::BufferAddress,
        step_mode: wgpu::VertexStepMode::Instance,
        attributes: &[
            wgpu::VertexAttribute {
                offset: 0,
                shader_location: 6,
                format: wgpu::VertexFormat::Float32x4,
            },
            wgpu::VertexAttribute {
                offset: mem::size_of::<[f32; 4]>() as wgpu::BufferAddress,
                shader_location: 7,
                format: wgpu::VertexFormat::Float32x4,
            },
            wgpu::VertexAttribute {
                offset: mem::size_of::<[f32; 4 * 2]>() as wgpu::BufferAddress,
                shader_location: 8,
                format: wgpu::VertexFormat::Float32x4,
            },
            wgpu::VertexAttribute {
                offset: mem::size_of::<[f32; 4 * 3]>() as wgpu::BufferAddress,
                shader_location: 9,
                format: wgpu::VertexFormat::Float32x4,
            },
            wgpu::VertexAttribute {
                offset: mem::size_of::<[f32; 4 * 4]>() as wgpu::BufferAddress,
                shader_location: 10,
                format: wgpu::VertexFormat::Uint32,
            },
        ],
    }
}

fn vertex_buffer_layout() -> wgpu::VertexBufferLayout<'static> {
    wgpu::VertexBufferLayout {
        array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
        step_mode: wgpu::VertexStepMode::Vertex,
        attributes: &[
            wgpu::VertexAttribute {
                offset: 0,
                shader_location: 0,
                format: wgpu::VertexFormat::Float32x3,
            },
            wgpu::VertexAttribute {
                offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                shader_location: 1,
                format: wgpu::VertexFormat::Float32x3,
            },
        ],
    }
}

fn instance_buffer_layout() -> wgpu::VertexBufferLayout<'static> {
    wgpu::VertexBufferLayout {
        array_stride: mem::size_of::<[[f32; 4]; 4]>() as wgpu::BufferAddress,
        step_mode: wgpu::VertexStepMode::Instance,
        attributes: &[
            wgpu::VertexAttribute {
                offset: 0,
                shader_location: 2,
                format: wgpu::VertexFormat::Float32x4,
            },
            wgpu::VertexAttribute {
                offset: mem::size_of::<[f32; 4]>() as wgpu::BufferAddress,
                shader_location: 3,
                format: wgpu::VertexFormat::Float32x4,
            },
            wgpu::VertexAttribute {
                offset: mem::size_of::<[f32; 4 * 2]>() as wgpu::BufferAddress,
                shader_location: 4,
                format: wgpu::VertexFormat::Float32x4,
            },
            wgpu::VertexAttribute {
                offset: mem::size_of::<[f32; 4 * 3]>() as wgpu::BufferAddress,
                shader_location: 5,
                format: wgpu::VertexFormat::Float32x4,
            },
        ],
    }
}

fn render_pipeline_new(
    device: &wgpu::Device,
    surface_config: &wgpu::SurfaceConfiguration,
) -> wgpu::RenderPipeline {
    let shader = device.create_shader_module(include_wgsl!("shader.wgsl"));
    let render_pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        label: Some("Render Pipeline Layout"),
        bind_group_layouts: &[&camera_bind_group_layout_new(&device)],
        push_constant_ranges: &[],
    });

    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: Some("Render Pipeline"),
        layout: Some(&render_pipeline_layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main",
            buffers: &[
                vertex_buffer_layout(),
                instance_buffer_layout(),
                wall_rotation_buffer_layout(),
            ],
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            targets: &[Some(wgpu::ColorTargetState {
                format: surface_config.format,
                blend: Some(wgpu::BlendState::REPLACE),
                write_mask: wgpu::ColorWrites::ALL,
            })],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw,
            //cull_mode: Some(wgpu::Face::Back),// set to None to see back and front face
            cull_mode: None, // set to None to see back and front face
            polygon_mode: wgpu::PolygonMode::Fill,
            unclipped_depth: false,
            conservative: false,
        },
        depth_stencil: Some(wgpu::DepthStencilState {
            format: wgpu::TextureFormat::Depth32Float,
            depth_write_enabled: true,
            depth_compare: wgpu::CompareFunction::Less,
            stencil: wgpu::StencilState::default(),
            bias: wgpu::DepthBiasState::default(),
        }),
        multisample: wgpu::MultisampleState {
            count: 1,
            mask: !0,
            alpha_to_coverage_enabled: false,
        },
        multiview: None,
    })
}

fn window() -> web_sys::Window {
    web_sys::window().expect("no global `window` exists")
}

fn request_animation_frame(f: &Closure<dyn FnMut()>) {
    window()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("should register `requestAnimationFrame` OK");
}

#[wasm_bindgen(start)]
pub fn run() {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init_with_level(log::Level::Info).unwrap();

    let event_loop = EventLoop::new().unwrap();
    let renderer = Rc::new(RefCell::new(Renderer::new(&event_loop)));
    let renderer_clone = renderer.clone();

    let closure = std::rc::Rc::new(std::cell::RefCell::new(None));
    let closure_clone = closure.clone();
    *closure_clone.borrow_mut() = Some(Closure::new(move || {
        let mut renderer = renderer_clone.borrow_mut();
        let time_now = Instant::now();
        let frame_duration = time_now.duration_since(renderer.time_last_frame);
        renderer.time_last_frame = time_now;
        renderer.transform_cube(frame_duration);
        renderer.render();
        request_animation_frame(closure.borrow().as_ref().unwrap());
    }));

    request_animation_frame(closure_clone.borrow().as_ref().unwrap());

    let _ = event_loop.run(move |event, target| {
        event::handle_events(event, target, &mut renderer.borrow_mut());
    });
}

pub struct Renderer {
    surface: wgpu::Surface<'static>,
    surface_config: wgpu::SurfaceConfiguration,
    device: wgpu::Device,
    queue: wgpu::Queue,
    vertex_buffer: wgpu::Buffer,
    camera_bind_group: wgpu::BindGroup,
    wall_rotation_buffer: wgpu::Buffer,
    instance_buffer: wgpu::Buffer,
    depth_texture_view: wgpu::TextureView,
    render_pipeline: wgpu::RenderPipeline,
    time_last_frame: Instant,
    cube: Cube,
}

impl Renderer {
    fn new(event_loop: &EventLoop<()>) -> Self {
        let instance = instance_new();
        let window = window::new(event_loop);
        let surface = instance.create_surface(window).unwrap();
        let adapter = adapter_new(&instance, &surface);
        let (device, queue) = device_queue_new(&adapter);
        let surface_config = surface
            .get_default_config(&adapter, *window::CANVAS_HEIGHT, *window::CANVAS_HEIGHT)
            .unwrap();
        surface.configure(&device, &surface_config);
        let cube = Cube::new();
        let vertex_buffer = vertex_buffer_new(&device, &cube.vertices);
        let camera_bind_group = camera_bind_group_new(&device);
        let wall_rotation_buffer = wall_rotation_buffer_new(&device);
        let instance_buffer = instance_buffer_new(&device, &cube.rotation);
        let depth_texture_view = depth_texture_view_new(&device);
        let render_pipeline = render_pipeline_new(&device, &surface_config);
        let time_last_frame = Instant::now();

        Renderer {
            surface,
            surface_config,
            device,
            queue,
            vertex_buffer,
            camera_bind_group,
            wall_rotation_buffer,
            instance_buffer,
            depth_texture_view,
            render_pipeline,
            cube,
            time_last_frame,
        }
    }

    fn render(&self) {
        let output = self.surface.get_current_texture().unwrap();
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
            label: Some("Render Pass"),
            color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                view: &view,
                resolve_target: None,
                ops: wgpu::Operations {
                    load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                    store: StoreOp::Store,
                },
            })],
            depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                view: &self.depth_texture_view,
                depth_ops: Some(wgpu::Operations {
                    load: wgpu::LoadOp::Clear(1.0),
                    store: StoreOp::Store,
                }),
                stencil_ops: None,
            }),
            timestamp_writes: None,
            occlusion_query_set: None,
        });

        render_pass.set_pipeline(&self.render_pipeline);
        render_pass.set_bind_group(0, &self.camera_bind_group, &[]);
        render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
        render_pass.set_vertex_buffer(1, self.instance_buffer.slice(..));
        render_pass.set_vertex_buffer(2, self.wall_rotation_buffer.slice(..));
        // first argument is range of vertices, second is range of instances
        render_pass.draw(0..self.cube.vertices.len() as u32, 0..1);
        // drop ownership of render pass before finishing encoder
        drop(render_pass);

        // submit takes iterator as parameter becaue it expects a series of commands
        self.queue.submit(std::iter::once(encoder.finish()));
        output.present();
    }

    fn transform_cube(&mut self, frame_duration: Duration) {
        self.cube.rotate_face(
            &self.device,
            &mut self.wall_rotation_buffer,
            &mut self.vertex_buffer,
            frame_duration,
        );
        self.cube
            .rotate(&self.device, &mut self.instance_buffer, frame_duration);
    }
}

struct Camera {
    eye: cgmath::Point3<f32>,
    target: cgmath::Point3<f32>,
    up: cgmath::Vector3<f32>,
    aspect: f32,
    fovy: f32,
    znear: f32,
    zfar: f32,
}

impl Camera {
    fn new() -> Self {
        Camera {
            eye: (0.0, 0.0, 3.5).into(),
            target: (0.0, 0.0, 0.0).into(),
            up: cgmath::Vector3::unit_y(),
            aspect: *window::CANVAS_HEIGHT as f32 / *window::CANVAS_HEIGHT as f32,
            fovy: 45.0,
            znear: 0.1,
            zfar: 100.0,
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
struct WallRotation {
    wall_rotation: [[f32; 4]; 4],
    rotation_type: u32,
}
