use once_cell::sync::Lazy;
use wasm_bindgen::JsCast;
use web_sys::{HtmlCanvasElement, Window as WebSysWindow};
use winit::{
    dpi::LogicalSize,
    event_loop::EventLoop,
    platform::web::WindowBuilderExtWebSys,
    window::{Window, WindowBuilder},
};

pub static CANVAS_HEIGHT: Lazy<u32> = Lazy::new(|| get_window_height());

fn window() -> WebSysWindow {
    web_sys::window().unwrap()
}

pub fn canvas() -> HtmlCanvasElement {
    window()
        .document()
        .unwrap()
        .get_element_by_id("canvas")
        .unwrap()
        .dyn_into::<HtmlCanvasElement>()
        .unwrap()
}

fn get_window_height() -> u32 {
    window().inner_height().unwrap().as_f64().unwrap() as u32
}

pub fn new(event_loop: &EventLoop<()>) -> Window {
    let canvas = canvas();
    WindowBuilder::new()
        .with_canvas(Some(canvas))
        .with_inner_size(LogicalSize::new(*CANVAS_HEIGHT, *CANVAS_HEIGHT))
        .build(event_loop)
        .unwrap()
}
