pub const VERTICES_COUNT: usize = 18 * 6 * 3;

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Vertex {
    position: [f32; 3],
    pub color: [f32; 3],
}

pub fn new() -> [Vertex; VERTICES_COUNT] {
    let one = -0.8;
    let two = -0.8 + 1.6 / 3.0;
    let three = 0.8 - 1.6 / 3.0;
    let four = 0.8;
    let z = 0.8;
    let a_front = [one, four, z];
    let b_front = [two, four, z];
    let c_front = [three, four, z];
    let d_front = [four, four, z];

    let e_front = [one, three, z];
    let f_front = [two, three, z];
    let g_front = [three, three, z];
    let h_front = [four, three, z];

    let i_front = [one, two, z];
    let j_front = [two, two, z];
    let k_front = [three, two, z];
    let l_front = [four, two, z];

    let m_front = [one, one, z];
    let n_front = [two, one, z];
    let o_front = [three, one, z];
    let p_front = [four, one, z];
    // back wall
    let a_back = [one, four, -z];
    let b_back = [two, four, -z];
    let c_back = [three, four, -z];
    let d_back = [four, four, -z];

    let e_back = [one, three, -z];
    let f_back = [two, three, -z];
    let g_back = [three, three, -z];
    let h_back = [four, three, -z];

    let i_back = [one, two, -z];
    let j_back = [two, two, -z];
    let k_back = [three, two, -z];
    let l_back = [four, two, -z];

    let m_back = [one, one, -z];
    let n_back = [two, one, -z];
    let o_back = [three, one, -z];
    let p_back = [four, one, -z];
    // left wall
    let a_left = [-z, four, one];
    let b_left = [-z, four, two];
    let c_left = [-z, four, three];
    let d_left = [-z, four, four];

    let e_left = [-z, three, one];
    let f_left = [-z, three, two];
    let g_left = [-z, three, three];
    let h_left = [-z, three, four];

    let i_left = [-z, two, one];
    let j_left = [-z, two, two];
    let k_left = [-z, two, three];
    let l_left = [-z, two, four];

    let m_left = [-z, one, one];
    let n_left = [-z, one, two];
    let o_left = [-z, one, three];
    let p_left = [-z, one, four];
    // right wall
    let a_right = [z, four, one];
    let b_right = [z, four, two];
    let c_right = [z, four, three];
    let d_right = [z, four, four];

    let e_right = [z, three, one];
    let f_right = [z, three, two];
    let g_right = [z, three, three];
    let h_right = [z, three, four];

    let i_right = [z, two, one];
    let j_right = [z, two, two];
    let k_right = [z, two, three];
    let l_right = [z, two, four];

    let m_right = [z, one, one];
    let n_right = [z, one, two];
    let o_right = [z, one, three];
    let p_right = [z, one, four];
    // top wall
    let a_top = [one, z, four];
    let b_top = [two, z, four];
    let c_top = [three, z, four];
    let d_top = [four, z, four];

    let e_top = [one, z, three];
    let f_top = [two, z, three];
    let g_top = [three, z, three];
    let h_top = [four, z, three];

    let i_top = [one, z, two];
    let j_top = [two, z, two];
    let k_top = [three, z, two];
    let l_top = [four, z, two];

    let m_top = [one, z, one];
    let n_top = [two, z, one];
    let o_top = [three, z, one];
    let p_top = [four, z, one];
    // bottom wall
    let a_bottom = [one, -z, four];
    let b_bottom = [two, -z, four];
    let c_bottom = [three, -z, four];
    let d_bottom = [four, -z, four];

    let e_bottom = [one, -z, three];
    let f_bottom = [two, -z, three];
    let g_bottom = [three, -z, three];
    let h_bottom = [four, -z, three];

    let i_bottom = [one, -z, two];
    let j_bottom = [two, -z, two];
    let k_bottom = [three, -z, two];
    let l_bottom = [four, -z, two];

    let m_bottom = [one, -z, one];
    let n_bottom = [two, -z, one];
    let o_bottom = [three, -z, one];
    let p_bottom = [four, -z, one];

    let white = [1.0, 1.0, 1.0];
    let yellow = [1.0, 1.0, 0.0];
    let red = [1.0, 0.0, 0.0];
    let orange = [1.0, 0.4, 0.0];
    let green = [0.0, 1.0, 0.0];
    let blue = [0.0, 0.0, 1.0];

    [
        // 1
        Vertex {
            position: a_front,
            color: white,
        },
        Vertex {
            position: e_front,
            color: white,
        },
        Vertex {
            position: b_front,
            color: white,
        },
        // 2
        Vertex {
            position: b_front,
            color: white,
        },
        Vertex {
            position: e_front,
            color: white,
        },
        Vertex {
            position: f_front,
            color: white,
        },
        // 3
        Vertex {
            position: c_front,
            color: white,
        },
        Vertex {
            position: b_front,
            color: white,
        },
        Vertex {
            position: f_front,
            color: white,
        },
        // 4
        Vertex {
            position: c_front,
            color: white,
        },
        Vertex {
            position: f_front,
            color: white,
        },
        Vertex {
            position: g_front,
            color: white,
        },
        // 5
        Vertex {
            position: d_front,
            color: white,
        },
        Vertex {
            position: c_front,
            color: white,
        },
        Vertex {
            position: g_front,
            color: white,
        },
        // 6
        Vertex {
            position: d_front,
            color: white,
        },
        Vertex {
            position: g_front,
            color: white,
        },
        Vertex {
            position: h_front,
            color: white,
        },
        // 7
        Vertex {
            position: f_front,
            color: white,
        },
        Vertex {
            position: e_front,
            color: white,
        },
        Vertex {
            position: i_front,
            color: white,
        },
        // 8
        Vertex {
            position: f_front,
            color: white,
        },
        Vertex {
            position: i_front,
            color: white,
        },
        Vertex {
            position: j_front,
            color: white,
        },
        // 9
        Vertex {
            position: g_front,
            color: white,
        },
        Vertex {
            position: f_front,
            color: white,
        },
        Vertex {
            position: j_front,
            color: white,
        },
        // 10
        Vertex {
            position: g_front,
            color: white,
        },
        Vertex {
            position: j_front,
            color: white,
        },
        Vertex {
            position: k_front,
            color: white,
        },
        // 11
        Vertex {
            position: h_front,
            color: white,
        },
        Vertex {
            position: g_front,
            color: white,
        },
        Vertex {
            position: k_front,
            color: white,
        },
        // 12
        Vertex {
            position: h_front,
            color: white,
        },
        Vertex {
            position: k_front,
            color: white,
        },
        Vertex {
            position: l_front,
            color: white,
        },
        // 13
        Vertex {
            position: j_front,
            color: white,
        },
        Vertex {
            position: i_front,
            color: white,
        },
        Vertex {
            position: m_front,
            color: white,
        },
        // 14
        Vertex {
            position: j_front,
            color: white,
        },
        Vertex {
            position: m_front,
            color: white,
        },
        Vertex {
            position: n_front,
            color: white,
        },
        // 15
        Vertex {
            position: k_front,
            color: white,
        },
        Vertex {
            position: j_front,
            color: white,
        },
        Vertex {
            position: n_front,
            color: white,
        },
        // 16
        Vertex {
            position: k_front,
            color: white,
        },
        Vertex {
            position: n_front,
            color: white,
        },
        Vertex {
            position: o_front,
            color: white,
        },
        // 17
        Vertex {
            position: l_front,
            color: white,
        },
        Vertex {
            position: k_front,
            color: white,
        },
        Vertex {
            position: o_front,
            color: white,
        },
        // 18
        Vertex {
            position: l_front,
            color: white,
        },
        Vertex {
            position: o_front,
            color: white,
        },
        Vertex {
            position: p_front,
            color: white,
        },
        // 1
        Vertex {
            position: a_back,
            color: yellow,
        },
        Vertex {
            position: e_back,
            color: yellow,
        },
        Vertex {
            position: b_back,
            color: yellow,
        },
        // 2
        Vertex {
            position: b_back,
            color: yellow,
        },
        Vertex {
            position: e_back,
            color: yellow,
        },
        Vertex {
            position: f_back,
            color: yellow,
        },
        // 3
        Vertex {
            position: c_back,
            color: yellow,
        },
        Vertex {
            position: b_back,
            color: yellow,
        },
        Vertex {
            position: f_back,
            color: yellow,
        },
        // 4
        Vertex {
            position: c_back,
            color: yellow,
        },
        Vertex {
            position: f_back,
            color: yellow,
        },
        Vertex {
            position: g_back,
            color: yellow,
        },
        // 5
        Vertex {
            position: d_back,
            color: yellow,
        },
        Vertex {
            position: c_back,
            color: yellow,
        },
        Vertex {
            position: g_back,
            color: yellow,
        },
        // 6
        Vertex {
            position: d_back,
            color: yellow,
        },
        Vertex {
            position: g_back,
            color: yellow,
        },
        Vertex {
            position: h_back,
            color: yellow,
        },
        // 7
        Vertex {
            position: f_back,
            color: yellow,
        },
        Vertex {
            position: e_back,
            color: yellow,
        },
        Vertex {
            position: i_back,
            color: yellow,
        },
        // 8
        Vertex {
            position: f_back,
            color: yellow,
        },
        Vertex {
            position: i_back,
            color: yellow,
        },
        Vertex {
            position: j_back,
            color: yellow,
        },
        // 9
        Vertex {
            position: g_back,
            color: yellow,
        },
        Vertex {
            position: f_back,
            color: yellow,
        },
        Vertex {
            position: j_back,
            color: yellow,
        },
        // 10
        Vertex {
            position: g_back,
            color: yellow,
        },
        Vertex {
            position: j_back,
            color: yellow,
        },
        Vertex {
            position: k_back,
            color: yellow,
        },
        // 11
        Vertex {
            position: h_back,
            color: yellow,
        },
        Vertex {
            position: g_back,
            color: yellow,
        },
        Vertex {
            position: k_back,
            color: yellow,
        },
        // 12
        Vertex {
            position: h_back,
            color: yellow,
        },
        Vertex {
            position: k_back,
            color: yellow,
        },
        Vertex {
            position: l_back,
            color: yellow,
        },
        // 13
        Vertex {
            position: j_back,
            color: yellow,
        },
        Vertex {
            position: i_back,
            color: yellow,
        },
        Vertex {
            position: m_back,
            color: yellow,
        },
        // 14
        Vertex {
            position: j_back,
            color: yellow,
        },
        Vertex {
            position: m_back,
            color: yellow,
        },
        Vertex {
            position: n_back,
            color: yellow,
        },
        // 15
        Vertex {
            position: k_back,
            color: yellow,
        },
        Vertex {
            position: j_back,
            color: yellow,
        },
        Vertex {
            position: n_back,
            color: yellow,
        },
        // 16
        Vertex {
            position: k_back,
            color: yellow,
        },
        Vertex {
            position: n_back,
            color: yellow,
        },
        Vertex {
            position: o_back,
            color: yellow,
        },
        // 17
        Vertex {
            position: l_back,
            color: yellow,
        },
        Vertex {
            position: k_back,
            color: yellow,
        },
        Vertex {
            position: o_back,
            color: yellow,
        },
        // 18
        Vertex {
            position: l_back,
            color: yellow,
        },
        Vertex {
            position: o_back,
            color: yellow,
        },
        Vertex {
            position: p_back,
            color: yellow,
        },
        // 1
        Vertex {
            position: a_left,
            color: red,
        },
        Vertex {
            position: e_left,
            color: red,
        },
        Vertex {
            position: b_left,
            color: red,
        },
        // 2
        Vertex {
            position: b_left,
            color: red,
        },
        Vertex {
            position: e_left,
            color: red,
        },
        Vertex {
            position: f_left,
            color: red,
        },
        // 3
        Vertex {
            position: c_left,
            color: red,
        },
        Vertex {
            position: b_left,
            color: red,
        },
        Vertex {
            position: f_left,
            color: red,
        },
        // 4
        Vertex {
            position: c_left,
            color: red,
        },
        Vertex {
            position: f_left,
            color: red,
        },
        Vertex {
            position: g_left,
            color: red,
        },
        // 5
        Vertex {
            position: d_left,
            color: red,
        },
        Vertex {
            position: c_left,
            color: red,
        },
        Vertex {
            position: g_left,
            color: red,
        },
        // 6
        Vertex {
            position: d_left,
            color: red,
        },
        Vertex {
            position: g_left,
            color: red,
        },
        Vertex {
            position: h_left,
            color: red,
        },
        // 7
        Vertex {
            position: f_left,
            color: red,
        },
        Vertex {
            position: e_left,
            color: red,
        },
        Vertex {
            position: i_left,
            color: red,
        },
        // 8
        Vertex {
            position: f_left,
            color: red,
        },
        Vertex {
            position: i_left,
            color: red,
        },
        Vertex {
            position: j_left,
            color: red,
        },
        // 9
        Vertex {
            position: g_left,
            color: red,
        },
        Vertex {
            position: f_left,
            color: red,
        },
        Vertex {
            position: j_left,
            color: red,
        },
        // 10
        Vertex {
            position: g_left,
            color: red,
        },
        Vertex {
            position: j_left,
            color: red,
        },
        Vertex {
            position: k_left,
            color: red,
        },
        // 11
        Vertex {
            position: h_left,
            color: red,
        },
        Vertex {
            position: g_left,
            color: red,
        },
        Vertex {
            position: k_left,
            color: red,
        },
        // 12
        Vertex {
            position: h_left,
            color: red,
        },
        Vertex {
            position: k_left,
            color: red,
        },
        Vertex {
            position: l_left,
            color: red,
        },
        // 13
        Vertex {
            position: j_left,
            color: red,
        },
        Vertex {
            position: i_left,
            color: red,
        },
        Vertex {
            position: m_left,
            color: red,
        },
        // 14
        Vertex {
            position: j_left,
            color: red,
        },
        Vertex {
            position: m_left,
            color: red,
        },
        Vertex {
            position: n_left,
            color: red,
        },
        // 15
        Vertex {
            position: k_left,
            color: red,
        },
        Vertex {
            position: j_left,
            color: red,
        },
        Vertex {
            position: n_left,
            color: red,
        },
        // 16
        Vertex {
            position: k_left,
            color: red,
        },
        Vertex {
            position: n_left,
            color: red,
        },
        Vertex {
            position: o_left,
            color: red,
        },
        // 17
        Vertex {
            position: l_left,
            color: red,
        },
        Vertex {
            position: k_left,
            color: red,
        },
        Vertex {
            position: o_left,
            color: red,
        },
        // 18
        Vertex {
            position: l_left,
            color: red,
        },
        Vertex {
            position: o_left,
            color: red,
        },
        Vertex {
            position: p_left,
            color: red,
        },
        // 1
        Vertex {
            position: a_right,
            color: orange,
        },
        Vertex {
            position: e_right,
            color: orange,
        },
        Vertex {
            position: b_right,
            color: orange,
        },
        // 2
        Vertex {
            position: b_right,
            color: orange,
        },
        Vertex {
            position: e_right,
            color: orange,
        },
        Vertex {
            position: f_right,
            color: orange,
        },
        // 3
        Vertex {
            position: c_right,
            color: orange,
        },
        Vertex {
            position: b_right,
            color: orange,
        },
        Vertex {
            position: f_right,
            color: orange,
        },
        // 4
        Vertex {
            position: c_right,
            color: orange,
        },
        Vertex {
            position: f_right,
            color: orange,
        },
        Vertex {
            position: g_right,
            color: orange,
        },
        // 5
        Vertex {
            position: d_right,
            color: orange,
        },
        Vertex {
            position: c_right,
            color: orange,
        },
        Vertex {
            position: g_right,
            color: orange,
        },
        // 6
        Vertex {
            position: d_right,
            color: orange,
        },
        Vertex {
            position: g_right,
            color: orange,
        },
        Vertex {
            position: h_right,
            color: orange,
        },
        // 7
        Vertex {
            position: f_right,
            color: orange,
        },
        Vertex {
            position: e_right,
            color: orange,
        },
        Vertex {
            position: i_right,
            color: orange,
        },
        // 8
        Vertex {
            position: f_right,
            color: orange,
        },
        Vertex {
            position: i_right,
            color: orange,
        },
        Vertex {
            position: j_right,
            color: orange,
        },
        // 9
        Vertex {
            position: g_right,
            color: orange,
        },
        Vertex {
            position: f_right,
            color: orange,
        },
        Vertex {
            position: j_right,
            color: orange,
        },
        // 10
        Vertex {
            position: g_right,
            color: orange,
        },
        Vertex {
            position: j_right,
            color: orange,
        },
        Vertex {
            position: k_right,
            color: orange,
        },
        // 11
        Vertex {
            position: h_right,
            color: orange,
        },
        Vertex {
            position: g_right,
            color: orange,
        },
        Vertex {
            position: k_right,
            color: orange,
        },
        // 12
        Vertex {
            position: h_right,
            color: orange,
        },
        Vertex {
            position: k_right,
            color: orange,
        },
        Vertex {
            position: l_right,
            color: orange,
        },
        // 13
        Vertex {
            position: j_right,
            color: orange,
        },
        Vertex {
            position: i_right,
            color: orange,
        },
        Vertex {
            position: m_right,
            color: orange,
        },
        // 14
        Vertex {
            position: j_right,
            color: orange,
        },
        Vertex {
            position: m_right,
            color: orange,
        },
        Vertex {
            position: n_right,
            color: orange,
        },
        // 15
        Vertex {
            position: k_right,
            color: orange,
        },
        Vertex {
            position: j_right,
            color: orange,
        },
        Vertex {
            position: n_right,
            color: orange,
        },
        // 16
        Vertex {
            position: k_right,
            color: orange,
        },
        Vertex {
            position: n_right,
            color: orange,
        },
        Vertex {
            position: o_right,
            color: orange,
        },
        // 17
        Vertex {
            position: l_right,
            color: orange,
        },
        Vertex {
            position: k_right,
            color: orange,
        },
        Vertex {
            position: o_right,
            color: orange,
        },
        // 18
        Vertex {
            position: l_right,
            color: orange,
        },
        Vertex {
            position: o_right,
            color: orange,
        },
        Vertex {
            position: p_right,
            color: orange,
        },
        // 1
        Vertex {
            position: a_top,
            color: green,
        },
        Vertex {
            position: e_top,
            color: green,
        },
        Vertex {
            position: b_top,
            color: green,
        },
        // 2
        Vertex {
            position: b_top,
            color: green,
        },
        Vertex {
            position: e_top,
            color: green,
        },
        Vertex {
            position: f_top,
            color: green,
        },
        // 3
        Vertex {
            position: c_top,
            color: green,
        },
        Vertex {
            position: b_top,
            color: green,
        },
        Vertex {
            position: f_top,
            color: green,
        },
        // 4
        Vertex {
            position: c_top,
            color: green,
        },
        Vertex {
            position: f_top,
            color: green,
        },
        Vertex {
            position: g_top,
            color: green,
        },
        // 5
        Vertex {
            position: d_top,
            color: green,
        },
        Vertex {
            position: c_top,
            color: green,
        },
        Vertex {
            position: g_top,
            color: green,
        },
        // 6
        Vertex {
            position: d_top,
            color: green,
        },
        Vertex {
            position: g_top,
            color: green,
        },
        Vertex {
            position: h_top,
            color: green,
        },
        // 7
        Vertex {
            position: f_top,
            color: green,
        },
        Vertex {
            position: e_top,
            color: green,
        },
        Vertex {
            position: i_top,
            color: green,
        },
        // 8
        Vertex {
            position: f_top,
            color: green,
        },
        Vertex {
            position: i_top,
            color: green,
        },
        Vertex {
            position: j_top,
            color: green,
        },
        // 9
        Vertex {
            position: g_top,
            color: green,
        },
        Vertex {
            position: f_top,
            color: green,
        },
        Vertex {
            position: j_top,
            color: green,
        },
        // 10
        Vertex {
            position: g_top,
            color: green,
        },
        Vertex {
            position: j_top,
            color: green,
        },
        Vertex {
            position: k_top,
            color: green,
        },
        // 11
        Vertex {
            position: h_top,
            color: green,
        },
        Vertex {
            position: g_top,
            color: green,
        },
        Vertex {
            position: k_top,
            color: green,
        },
        // 12
        Vertex {
            position: h_top,
            color: green,
        },
        Vertex {
            position: k_top,
            color: green,
        },
        Vertex {
            position: l_top,
            color: green,
        },
        // 13
        Vertex {
            position: j_top,
            color: green,
        },
        Vertex {
            position: i_top,
            color: green,
        },
        Vertex {
            position: m_top,
            color: green,
        },
        // 14
        Vertex {
            position: j_top,
            color: green,
        },
        Vertex {
            position: m_top,
            color: green,
        },
        Vertex {
            position: n_top,
            color: green,
        },
        // 15
        Vertex {
            position: k_top,
            color: green,
        },
        Vertex {
            position: j_top,
            color: green,
        },
        Vertex {
            position: n_top,
            color: green,
        },
        // 16
        Vertex {
            position: k_top,
            color: green,
        },
        Vertex {
            position: n_top,
            color: green,
        },
        Vertex {
            position: o_top,
            color: green,
        },
        // 17
        Vertex {
            position: l_top,
            color: green,
        },
        Vertex {
            position: k_top,
            color: green,
        },
        Vertex {
            position: o_top,
            color: green,
        },
        // 18
        Vertex {
            position: l_top,
            color: green,
        },
        Vertex {
            position: o_top,
            color: green,
        },
        Vertex {
            position: p_top,
            color: green,
        },
        // 1
        Vertex {
            position: a_bottom,
            color: blue,
        },
        Vertex {
            position: e_bottom,
            color: blue,
        },
        Vertex {
            position: b_bottom,
            color: blue,
        },
        // 2
        Vertex {
            position: b_bottom,
            color: blue,
        },
        Vertex {
            position: e_bottom,
            color: blue,
        },
        Vertex {
            position: f_bottom,
            color: blue,
        },
        // 3
        Vertex {
            position: c_bottom,
            color: blue,
        },
        Vertex {
            position: b_bottom,
            color: blue,
        },
        Vertex {
            position: f_bottom,
            color: blue,
        },
        // 4
        Vertex {
            position: c_bottom,
            color: blue,
        },
        Vertex {
            position: f_bottom,
            color: blue,
        },
        Vertex {
            position: g_bottom,
            color: blue,
        },
        // 5
        Vertex {
            position: d_bottom,
            color: blue,
        },
        Vertex {
            position: c_bottom,
            color: blue,
        },
        Vertex {
            position: g_bottom,
            color: blue,
        },
        // 6
        Vertex {
            position: d_bottom,
            color: blue,
        },
        Vertex {
            position: g_bottom,
            color: blue,
        },
        Vertex {
            position: h_bottom,
            color: blue,
        },
        // 7
        Vertex {
            position: f_bottom,
            color: blue,
        },
        Vertex {
            position: e_bottom,
            color: blue,
        },
        Vertex {
            position: i_bottom,
            color: blue,
        },
        // 8
        Vertex {
            position: f_bottom,
            color: blue,
        },
        Vertex {
            position: i_bottom,
            color: blue,
        },
        Vertex {
            position: j_bottom,
            color: blue,
        },
        // 9
        Vertex {
            position: g_bottom,
            color: blue,
        },
        Vertex {
            position: f_bottom,
            color: blue,
        },
        Vertex {
            position: j_bottom,
            color: blue,
        },
        // 10
        Vertex {
            position: g_bottom,
            color: blue,
        },
        Vertex {
            position: j_bottom,
            color: blue,
        },
        Vertex {
            position: k_bottom,
            color: blue,
        },
        // 11
        Vertex {
            position: h_bottom,
            color: blue,
        },
        Vertex {
            position: g_bottom,
            color: blue,
        },
        Vertex {
            position: k_bottom,
            color: blue,
        },
        // 12
        Vertex {
            position: h_bottom,
            color: blue,
        },
        Vertex {
            position: k_bottom,
            color: blue,
        },
        Vertex {
            position: l_bottom,
            color: blue,
        },
        // 13
        Vertex {
            position: j_bottom,
            color: blue,
        },
        Vertex {
            position: i_bottom,
            color: blue,
        },
        Vertex {
            position: m_bottom,
            color: blue,
        },
        // 14
        Vertex {
            position: j_bottom,
            color: blue,
        },
        Vertex {
            position: m_bottom,
            color: blue,
        },
        Vertex {
            position: n_bottom,
            color: blue,
        },
        // 15
        Vertex {
            position: k_bottom,
            color: blue,
        },
        Vertex {
            position: j_bottom,
            color: blue,
        },
        Vertex {
            position: n_bottom,
            color: blue,
        },
        // 16
        Vertex {
            position: k_bottom,
            color: blue,
        },
        Vertex {
            position: n_bottom,
            color: blue,
        },
        Vertex {
            position: o_bottom,
            color: blue,
        },
        // 17
        Vertex {
            position: l_bottom,
            color: blue,
        },
        Vertex {
            position: k_bottom,
            color: blue,
        },
        Vertex {
            position: o_bottom,
            color: blue,
        },
        // 18
        Vertex {
            position: l_bottom,
            color: blue,
        },
        Vertex {
            position: o_bottom,
            color: blue,
        },
        Vertex {
            position: p_bottom,
            color: blue,
        },
    ]
}
