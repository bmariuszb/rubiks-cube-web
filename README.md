# Rubik's cube
Simulation of a standard 3x3 Rubik's cube in a browser using WebGL2  
Available [online](https://ezlearn.ix.tc/showcase/rubiks-cube)

## Dependencies
Optional  
`cargo install trunk` - for convenient development

## Run
`ln deploy/index.html index.html` - create hard link for trunk  
`trunk serve --open` - tracks file changes, automatically recompiles project and reloads webpage
