# Deployment
## Dependencies
```console
rustup target install wasm32-unknown-unknown
cargo install wasm-bindgen-cli
```

## Build and create artifacts
```console
cargo build --target wasm32-unknown-unknown --release
wasm-bindgen --target web --out-dir deploy/ --no-typescript target/wasm32-unknown-unknown/release/rubiks_cube_web.wasm
```

## Deploy
Serve index.html .wasm and .js files from `deploy/` directory with your favourite HTTP/HTTPS server, remember to adjust the path to .js file in index.html
